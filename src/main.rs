pub mod file_type;
pub mod highlighting;
pub mod row;
pub mod utils;
pub mod terminal;
pub mod editor;
pub mod document;

fn main() {
    editor::Editor::default().run();
}
