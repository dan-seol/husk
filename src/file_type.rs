use crate::highlighting::HighlightingOptions;

// Stores recognized file types
pub struct FileType {
    name: String,
    highlighting_options: HighlightingOptions,
}

/// Will have a default value
impl Default for FileType {
    fn default() -> Self {
        Self {
            name: String::from("Unknown filetype"),
            highlighting_options: HighlightingOptions::default(),
        }
    }
}

impl FileType {
    pub fn name(&self) -> String {
        self.name.clone()
    }

    pub fn highlighting_options(&self) -> &HighlightingOptions {
        &self.highlighting_options
    }

    pub fn from(file_name: &str) -> Self {
        if file_name.ends_with(".rs") {
            return Self {
                name: String::from("Rust"),
                highlighting_options: HighlightingOptions::get_highlighting_options_for_rust(),
            };
        }
        else if file_name.ends_with(".go") {
            return Self {
                name: String::from("Go"),
                highlighting_options: HighlightingOptions::get_highlighting_options_for_go(),
            };
        }
        else if file_name.ends_with(".jl") {
            return Self {
                name: String::from("Julia"),
                highlighting_options: HighlightingOptions::get_highlighting_options_for_julia(),
            };
        }
        Self::default()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Given: FileType with implemented Default
    /// When: a default instance of FileType is created
    /// Then: the instance contains default values as expected
    #[test]
    fn test_default_file_type() {
        let default_file_type: FileType = FileType::default();
        assert_eq!(String::from("Unknown filetype"), default_file_type.name());
        assert!(!default_file_type.highlighting_options().numbers());
        assert!(!default_file_type.highlighting_options().strings());
        assert!(!default_file_type.highlighting_options().asterisks());
        assert!(!default_file_type.highlighting_options().characters());
        assert!(!default_file_type.highlighting_options().comments());
        assert!(!default_file_type.highlighting_options().multiline_comments());
        assert!(default_file_type.highlighting_options().primary_keywords().is_empty());
        assert!(default_file_type.highlighting_options().secondary_keywords().is_empty());
    }

    /// Given: FileType with .rs file name
    /// When: FileType is instantiated
    /// Then: the instance contains values as expected
    #[test]
    fn test_file_type_from_file_name_rust() {
        let file_type: FileType = FileType::from("rust_file.rs");
        assert_eq!(String::from("Rust"), file_type.name());
        assert!(file_type.highlighting_options().numbers());
        assert!(file_type.highlighting_options().strings());
        assert!(!file_type.highlighting_options().asterisks());
        assert!(file_type.highlighting_options().characters());
        assert!(file_type.highlighting_options().comments());
        assert!(file_type.highlighting_options().multiline_comments());
        assert!(!file_type.highlighting_options().primary_keywords().is_empty());
        assert!(!file_type.highlighting_options().secondary_keywords().is_empty());
    }

    /// Given: FileType with .go file name
    /// When: FileType is instantiated
    /// Then: the instance contains values as expected
    #[test]
    fn test_file_type_from_file_name_go() {
        let file_type: FileType = FileType::from("go_file.go");
        assert_eq!(String::from("Go"), file_type.name());
        assert!(file_type.highlighting_options().numbers());
        assert!(file_type.highlighting_options().strings());
        assert!(file_type.highlighting_options().asterisks());
        assert!(file_type.highlighting_options().characters());
        assert!(file_type.highlighting_options().comments());
        assert!(file_type.highlighting_options().multiline_comments());
        assert!(!file_type.highlighting_options().primary_keywords().is_empty());
        assert!(!file_type.highlighting_options().secondary_keywords().is_empty());
    }

    #[test]
    fn test_file_type_from_file_name_julia() {
        let file_type: FileType = FileType::from("julia_file.jl");
        assert_eq!(String::from("Julia"), file_type.name());
        assert!(file_type.highlighting_options().numbers());
        assert!(file_type.highlighting_options().strings());
        assert!(!file_type.highlighting_options().asterisks());
        assert!(file_type.highlighting_options().characters());
        assert!(file_type.highlighting_options().comments());
        assert!(file_type.highlighting_options().multiline_comments());
        assert!(!file_type.highlighting_options().primary_keywords().is_empty());
        assert!(!file_type.highlighting_options().secondary_keywords().is_empty());
    }
}