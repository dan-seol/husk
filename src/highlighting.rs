use crossterm::style::Color;

/// Stores custom highlighting options
#[derive(Default)]
pub struct HighlightingOptions {
    numbers: bool,
    strings: bool,
    asterisks: bool,
    characters: bool,
    comments: bool,
    multiline_comments: bool,
    primary_keywords: Vec<String>,
    secondary_keywords: Vec<String>,
}

impl HighlightingOptions {
    pub fn numbers(&self) -> bool {
        self.numbers
    }

    pub fn strings(&self) -> bool {
        self.strings
    }

    pub fn asterisks(&self) -> bool {
        self.asterisks
    }

    pub fn characters(&self) -> bool {
        self.characters
    }

    pub fn comments(&self) -> bool {
        self.comments
    }

    pub fn multiline_comments(&self) -> bool {
        self.multiline_comments
    }

    pub fn primary_keywords(&self) -> &Vec<String> {
        &self.primary_keywords
    }

    pub fn secondary_keywords(&self) -> &Vec<String> {
        &self.secondary_keywords
    }

    pub fn get_highlighting_options_for_rust() -> HighlightingOptions {
        HighlightingOptions {
            numbers: true,
            strings: true,
            asterisks: false,
            characters: true,
            comments: true,
            multiline_comments: true,
            primary_keywords: get_primary_keywords_for_rust(),
            secondary_keywords: get_secondary_keywords_for_rust(),
        }
    }

    pub fn get_highlighting_options_for_go() -> HighlightingOptions {
        HighlightingOptions {
            numbers: true,
            strings: true,
            asterisks: true,
            characters: true,
            comments: true,
            multiline_comments: true,
            primary_keywords: get_primary_keywords_for_go(),
            secondary_keywords: get_secondary_keywords_for_go(),
        }
    }

    pub fn get_highlighting_options_for_julia() -> HighlightingOptions {
        HighlightingOptions {
            numbers: true,
            strings: true,
            asterisks: false,
            characters: true,
            comments: true,
            multiline_comments: true,
            primary_keywords: get_primary_keywords_for_julia(),
            secondary_keywords: get_secondary_keywords_for_julia(),
        }
    }
}


fn get_primary_keywords_for_rust() -> Vec<String> {
    return vec![
        "as".to_string(),
        "break".to_string(),
        "const".to_string(),
        "continue".to_string(),
        "crate".to_string(),
        "else".to_string(),
        "enum".to_string(),
        "extern".to_string(),
        "false".to_string(),
        "fn".to_string(),
        "for".to_string(),
        "if".to_string(),
        "impl".to_string(),
        "in".to_string(),
        "let".to_string(),
        "loop".to_string(),
        "match".to_string(),
        "mod".to_string(),
        "move".to_string(),
        "mut".to_string(),
        "pub".to_string(),
        "ref".to_string(),
        "return".to_string(),
        "self".to_string(),
        "Self".to_string(),
        "static".to_string(),
        "struct".to_string(),
        "super".to_string(),
        "trait".to_string(),
        "true".to_string(),
        "type".to_string(),
        "unsafe".to_string(),
        "use".to_string(),
        "where".to_string(),
        "while".to_string(),
        "dyn".to_string(),
        "abstract".to_string(),
        "become".to_string(),
        "box".to_string(),
        "do".to_string(),
        "final".to_string(),
        "macro".to_string(),
        "override".to_string(),
        "priv".to_string(),
        "typeof".to_string(),
        "unsized".to_string(),
        "virtual".to_string(),
        "yield".to_string(),
        "async".to_string(),
        "await".to_string(),
        "try".to_string(),
    ];
}

fn get_secondary_keywords_for_rust() -> Vec<String> {
    return vec![
        "bool".to_string(),
        "char".to_string(),
        "i8".to_string(),
        "i16".to_string(),
        "i32".to_string(),
        "i64".to_string(),
        "isize".to_string(),
        "u8".to_string(),
        "u16".to_string(),
        "u32".to_string(),
        "u64".to_string(),
        "usize".to_string(),
        "f32".to_string(),
        "f64".to_string(),
    ];
}

fn get_primary_keywords_for_go() -> Vec<String> {
    return vec![
        "break".to_string(),
        "case".to_string(),
        "chan".to_string(),
        "const".to_string(),
        "continue".to_string(),
        "default".to_string(),
        "defer".to_string(),
        "else".to_string(),
        "fallthrough".to_string(),
        "for".to_string(),
        "func".to_string(),
        "go".to_string(),
        "goto".to_string(),
        "if".to_string(),
        "import".to_string(),
        "interface".to_string(),
        "iota".to_string(),
        "map".to_string(),
        "package".to_string(),
        "range".to_string(),
        "return".to_string(),
        "select".to_string(),
        "struct".to_string(),
        "switch".to_string(),
        "type".to_string(),
        "var".to_string(),
    ];
}

fn get_secondary_keywords_for_go() -> Vec<String> {
    return vec![
        "uint".to_string(),
        "uint8".to_string(),
        "uint16".to_string(),
        "uint32".to_string(),
        "uint64".to_string(),
        "int".to_string(),
        "int8".to_string(),
        "int16".to_string(),
        "int32".to_string(),
        "int64".to_string(),
        "float32".to_string(),
        "float64".to_string(),
        "complex64".to_string(),
        "complex128".to_string(),
        "byte".to_string(),
        "rune".to_string(),
    ];
}

fn get_primary_keywords_for_julia() -> Vec<String> {
    return vec![
        "baremodule".to_string(),
        "begin".to_string(),
        "break".to_string(),
        "catch".to_string(),
        "const".to_string(),
        "continue".to_string(),
        "do".to_string(),
        "else".to_string(),
        "elseif".to_string(),
        "end".to_string(),
        "export".to_string(),
        "false".to_string(),
        "finally".to_string(),
        "for".to_string(),
        "function".to_string(),
        "global".to_string(),
        "if".to_string(),
        "import".to_string(),
        "let".to_string(),
        "local".to_string(),
        "macro".to_string(),
        "module".to_string(),
        "quote".to_string(),
        "return".to_string(),
        "struct".to_string(),
        "true".to_string(),
        "try".to_string(),
        "using".to_string(),
        "while".to_string(),
        "abstract type".to_string(),
        "mutable struct".to_string(),
        "primitive type".to_string(),
        "where".to_string(),
        "in".to_string(),
        "isa".to_string(),
    ];
}

fn get_secondary_keywords_for_julia() -> Vec<String> {
    return vec![
        "Number".to_string(),
        "Real".to_string(),
        "AbstractFloat".to_string(),
        "Integer".to_string(),
        "Signed".to_string(),
        "Unsigned".to_string(),
        "Float16".to_string(),
        "Float32".to_string(),
        "Float64".to_string(),
        "Bool".to_string(),
        "Char".to_string(),
        "Int8".to_string(),
        "UInt8".to_string(),
        "Int16".to_string(),
        "UInt16".to_string(),
        "Int32".to_string(),
        "UInt32".to_string(),
        "Int64".to_string(),
        "UInt64".to_string(),
        "Int128".to_string(),
        "UInt128".to_string(),
        "Union".to_string(),
        "Function".to_string(),
    ];
}


/// Defines available highlighting types
#[derive(PartialEq, Clone, Copy, Debug)]
pub enum HighlightingType {
    None,
    Number,
    Match,
    String,
    Asterisks,
    Character,
    Comment,
    MultilineComment,
    PrimaryKeywords,
    SecondaryKeywords,
}

impl HighlightingType {
    pub fn to_color(self) -> Color {
        match self {
            HighlightingType::Number => Color::Rgb{r: 192, g: 232, b: 127},
            HighlightingType::Match => Color::Rgb{r: 38, g: 139, b: 210},
            HighlightingType::String => Color::Rgb{r: 211, g: 54, b: 130},
            HighlightingType::Asterisks => Color::Rgb{r: 232, g: 127, b: 145},
            HighlightingType::Character => Color::Rgb{r: 108, g: 113, b: 196},
            HighlightingType::Comment | HighlightingType::MultilineComment => Color::Rgb{r: 133, g: 153, b: 0},
            HighlightingType::PrimaryKeywords => Color::Rgb{r: 247, g: 29, b: 153},
            HighlightingType::SecondaryKeywords => Color::Rgb{r: 42, g: 161, b: 152},
            _ => Color::Rgb{r: 255, g: 255, b: 255},
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    /// Given: HighlightingOptions with derived Default
    /// When: a default instance of HighlightingOptions is created
    /// Then: the instance contains default values as expected
    #[test]
    fn test_default_highlighting_options() {
        let default_options: HighlightingOptions = HighlightingOptions::default();
        assert!(!default_options.numbers());
        assert!(!default_options.strings());
        assert!(!default_options.asterisks());
        assert!(!default_options.characters());
        assert!(!default_options.comments());
        assert!(!default_options.multiline_comments());
        assert!(default_options.primary_keywords().is_empty());
        assert!(default_options.secondary_keywords().is_empty());
    }

    /// Given: HighlightingType with to_color method defined
    /// When: any instance of possible HighlightingType is created
    /// Then: to_color method returns values as expected
    #[test]
    fn test_highlighting_type_to_color() {
        assert_eq!(Color::Rgb{r: 255, g: 255, b: 255}, HighlightingType::None.to_color());
        assert_eq!(Color::Rgb{r: 192, g: 232, b: 127}, HighlightingType::Number.to_color());
        assert_eq!(Color::Rgb{r: 38, g: 139, b: 210}, HighlightingType::Match.to_color());
        assert_eq!(Color::Rgb{r: 211, g: 54, b: 130}, HighlightingType::String.to_color());
        assert_eq!(Color::Rgb{r: 232, g: 127, b: 145}, HighlightingType::Asterisks.to_color());
        assert_eq!(Color::Rgb{r: 108, g: 113, b: 196}, HighlightingType::Character.to_color());
        assert_eq!(Color::Rgb{r: 133, g: 153, b: 0}, HighlightingType::Comment.to_color());
        assert_eq!(Color::Rgb{r: 133, g: 153, b: 0}, HighlightingType::MultilineComment.to_color());
        assert_eq!(Color::Rgb{r: 247, g: 29, b: 153}, HighlightingType::PrimaryKeywords.to_color());
        assert_eq!(Color::Rgb{r: 42, g: 161, b: 152}, HighlightingType::SecondaryKeywords.to_color());

    }
}